import { showModal } from './modal'
import { createFighterImage } from '../fighterPreview'

export function showWinnerModal(fighter) {
  // call showModal function 
  console.log("in showWinnerModal");
  console.log(fighter);
  const resalt = ` ${fighter.name} wins `;
  const title = "Game over";
  showModal({ title });
}
